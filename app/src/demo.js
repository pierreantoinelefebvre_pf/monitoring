import moment from 'moment'

export const stats = [{
  header: '8390',
  text: 'Visitors'
}, {
  header: '30%',
  text: 'Referrals'
}, {
  header: '70%',
  text: 'Organic'
}]

export const timeline = [{
  icon: 'fa-stop',
  color: 'green',
  title: 'Job pfdc-kr-importdeliveryconfirmation END',
  time: moment().endOf('day').fromNow()
}, {
  icon: 'fa-play',
  color: 'blue',
  title: 'Job pfdc-kr-importdeliveryconfirmation START',
  time: moment().endOf('month').fromNow()
}, {
  icon: 'fa-pause',
  color: 'red',
  title: 'Job pfdc-kr-importdeliveryconfirmation Finished with ERRORS',
  time: moment().endOf('day').fromNow(),
  body: 'Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...',
  buttons: [
    {
      type: 'primary',
      message: 'See logs details',
      href: 'https://github.com/misterGF/CoPilot',
      target: '_blank'
    },
    {
      type: 'primary',
      message: 'Run again',
      href: 'https://github.com/misterGF/CoPilot',
      target: '_blank'
    }
  ]
}]

export const jobsSchedulesDefinition = [{
  name: 'pfdc-importmastercatalog toto',
  status: 'success',
  icon: 'globe',
  description: 'Import master catalog'
}, {
  name: 'pfdc-kr-backinstock',
  status: 'danger',
  icon: 'database',
  description: 'Back in stock notification'
}, {
  name: ' pfdc-kr-disableinactivecustomers',
  status: 'info',
  icon: 'file-code-o',
  description: 'Disable inactive customer'
}, {
  name: 'pfdc-kr-enquiries',
  status: 'success',
  icon: 'key',
  description: 'PFDC Enquiries'
}, {
  name: 'pfdc-kr-exportorders',
  status: 'success',
  icon: 'home',
  description: 'Export order to OMS'
}, {
  name: 'pfdc-kr-importdeliveryconfirmation',
  status: 'warning',
  icon: 'backward',
  description: 'Order notification status'
}, {
  name: 'pfdc-importmastercatalog',
  status: 'success',
  icon: 'globe',
  description: 'Import master catalog'
}, {
  name: 'pfdc-kr-backinstock',
  status: 'danger',
  icon: 'database',
  description: 'Back in stock notification'
}, {
  name: ' pfdc-kr-disableinactivecustomers',
  status: 'info',
  icon: 'file-code-o',
  description: 'Disable inactive customer'
}, {
  name: 'pfdc-kr-enquiries',
  status: 'success',
  icon: 'key',
  description: 'PFDC Enquiries'
}, {
  name: 'pfdc-kr-exportorders',
  status: 'success',
  icon: 'home',
  description: 'Export order to OMS'
}, {
  name: 'pfdc-kr-importdeliveryconfirmation',
  status: 'warning',
  icon: 'backward',
  description: 'Order notification status'
}]
