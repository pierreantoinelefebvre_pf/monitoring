import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import TimelineView from './components/views/Timeline.vue'
import JobsDefinitionView from './components/views/jobs/JobsDefinition.vue'
import JobsHistoryView from './components/views/jobs/JobsHistory.vue'
import JobsExecutionView from './components/views/jobs/JobsExecution.vue'
import JobsDetailsView from './components/views/jobs/JobDetails.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: 'Overview of environment'}
      }, {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: false}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      }, {
        path: 'timeline',
        component: TimelineView,
        name: 'Timeline',
        meta: {description: 'All actions and notification related the web ecommerce'}
      }, {
        path: 'jobs-execution',
        component: JobsExecutionView,
        name: 'JobsExecution',
        meta: {description: 'Run a job manually'}
      }, {
        path: 'jobs-definition',
        component: JobsDefinitionView,
        name: 'Jobs Schedules',
        meta: {description: 'List of Jobs schedules'},
        children: [
          {
            path: 'details',
            component: JobsDetailsView,
            name: 'Jobs Details',
            meta: {description: 'See the job details'}
          }
        ]
      }, {
        path: 'jobs-history',
        component: JobsHistoryView,
        name: 'Jobs History',
        meta: {description: 'Job history / last run'}
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
